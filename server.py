# -*- coding utf-8 -*-
import web
import os
import random
import string
import urllib2
from time import gmtime, strftime

urls = ('/', 'multi',
        '/api', 'api',
        '/api/upload/(.*)', 'transload',
        '/api/delete(.*)', 'delete',
        '/privacy', 'privacy',
        '/about', 'about',
        '/s/(.*)', 'imageviewer',
        '/new', 'new',
        '/multi', 'multi',
    )

app = web.application(urls, globals()) 
web.config.debug = False
filedir = '/var/www/asparg.us/i'
imagelist = []

render = web.template.render("html/", base="base", globals={"context": "session"}, cache=False)
render_plain = web.template.render("html/", globals={"context": "session"}, cache=False)

def curtime():
    response = strftime("%Y-%m-%d %H:%M:%S")
    return response

def writelog(entry):
    f = open("logfile.txt", "a")
    entry = curtime() + " | " + web.ctx.ip + ": " + entry + "\n"
    f.write(entry)
    f.close()
    return

def searchlog(string):
    f = open("logfile.txt")
    for line in f:
        if string in line:
            return line
    return False

def genfilename(extension, uploadtype):
    tmpext = extension.split("?")[0]
    ext = tmpext.split(".")[-1]
    foo = random.SystemRandom()
    length = 6
    chars = string.letters + string.digits
    if uploadtype == "multi":
        fn = ''.join(foo.choice(chars) for _ in xrange(length)) + "." + extension
    if uploadtype == "single":
        fn = ''.join(foo.choice(chars) for _ in xrange(length)) + "." + ext
    return fn

def getimagetype(firstbytes):
    if "PNG" in firstbytes:
        extension = "png"
    elif "JFIF" in firstbytes:
        extension = "jpg"
    elif "GIF" in firstbytes:
        extension = "gif"
    else:
        extension = "unknown"
    return extension

def upload(x):
    try:
        if 'myfile' in x: # to check if the file-object is created
            filepath=x.myfile.filename.replace('\\','/') # replaces the windows-style slashes with linux ones.
            filename=genfilename(filepath, "single")
            writelog("Trying to upload " + filepath + " as " + filename)
            fout = open(filedir +'/'+ filename,'w') # creates the file where the uploaded file should be stored
            fout.write(x.myfile.file.read()) # writes the uploaded file to the newly created file.
            fout.close() # closes the file, upload complete.
        pathtofile = "i/" + str(filename)
        writelog(pathtofile + " successfylly uploaded!")
        return pathtofile
    except:
        writelog("Upload failed!")
        return False

def download_photo(img_url):
    try:
        filename = genfilename(img_url, "single")
        writelog("Trying to transload " + img_url + " as " + filename)
        img = urllib2.urlopen(img_url)
        if img.headers.maintype == 'image':
            buf = img.read()
            path = filedir 
            file_path = "%s/%s" % (path, filename)
            downloaded_image = file(file_path, "wb")
            downloaded_image.write(buf)
            downloaded_image.close()
            img.close()
            pathtofile = "/i/" + str(filename)
            writelog("Transload success!")
            return pathtofile
        else:
            writelog("Transloading failed, not an image")
            return False
    except:
        return False

def delete(filename):
    fullpath = filedir + "/" + filename
    writelog("Trying to delete " + fullpath)
    if os.path.exists(fullpath):
        verification = searchlog("as "+filename)
        if web.ctx.ip in verification:
            writelog("IP verification succeeded.")
            os.remove(filedir + "/" + filename)
            writelog(filename + " removed")
            return True
        else:
            writelog("IP verification failed!")
    else:
        return False

def sorted_ls(path):
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    return list(sorted(os.listdir(path), key=mtime, reverse=True))[:10]

class api:
    def GET(self):
        return render.api()

class privacy:
    def GET(self):
        return render.privacy()

class about:
    def GET(self):
        return render.about()

class new:
    def GET(self):
        writelog("GET / newest files")
        files = sorted_ls(filedir)
        return render.new(files)

class imageviewer:
    def GET(self, url):
        writelog("GET / image viewer")
        url = url.split("/")[-1]
        url = "http://asparg.us/i/" + url
        return render_plain.imageview(url)

class transload:
    def GET(self, url):
        params = web.input()
        download = download_photo(params.url)
        if download == False:
            return render.error("Unable to transload file")
        else:
            filepath = "http://asparg.us" + download
            forumlink = "[IMG]" + filepath + "[/IMG]"
            fburl = "http://asparg.us/s/" + filepath.split("/")[-1]
            return render.image(filepath, forumlink, fburl)

class index:
    def GET(self):
        writelog("GET / index")
        web.header("Content-Type","text/html; charset=utf-8")
        return render.index()

    def POST(self):
        writelog("POST / index")
        webinput = web.input()
        if "myfile" in webinput:
            x = web.input(myfile={})
            result = upload(x)
            if result == False:
                return render.error("crap")
            else:
                filepath = "http://asparg.us/" + result
                forumlink = "[IMG]" + filepath + "[/IMG]"
                fburl = "http://asparg.us/s/" + filepath.split("/")[-1]
                return render.image(filepath, forumlink, fburl)
        elif "delete" in webinput:
            writelog("POST / Request to delete file")
            if delete(webinput.delete.split("/")[-1]):
                return render.error("Yay, image deleted!")
            else:
                return render.error("Image not found (already deleted?).")
        else:
            return render.error("Ugulus bugulus")

def saveimage(filedata):
    i = filedata
    extension = getimagetype(str(i)[0:14])
    if extension != "unknown":
        fn = genfilename(extension, "multi") 
        f = open(filedir + "/" + fn, "w")
        writelog("Trying to upload image as " + fn)
        f.write(i)
        f.close()
        print "file hopefully written to: " + filedir + "/" + fn
        return fn
    else:
        print "no images attached or unknown type"
        return False
    return

class multi:
    def GET(self):
        writelog("Multiupload")
        return render.multi()
    def POST(self):
        global imagelist
        writelog("POST / multi")
        if not "delete" in web.input():
            webinput = web.input(filedata=[])
            if len(webinput.filedata)>0:
                imagelist = []
                for i in webinput.filedata:
                    d = {}
                    print "there are images. how many?",
                    print len(webinput.filedata)
                    fn = saveimage(i)
                    if fn:
                        filepath = "http://asparg.us/i/" + fn
                        d['direct'] = filepath
                        d['forumlink'] = "[IMG]" + filepath + "[/IMG]"
                        d['fburl'] = "http://asparg.us/s/" + filepath.split("/")[-1]
                        imagelist.append(d)
                print "image list: ",
                print imagelist
                return render.multiimage(imagelist, None)
        elif "delete" in web.input():
            webinput = web.input()
            file2remove = webinput.delete
            print "got request to remove ",
            print file2remove
            fn = file2remove.split("/")[-1]
            if delete(fn):
            #if delete(webinput.delete.split("/")[-1]):
                conv2list = eval(webinput.hidden)
                print "converted list:"
                print conv2list
                fileurl = file2remove.split()[-1]
                print "going to delete from list:"
                print fileurl
                imagelist = [x for x in conv2list if not (fileurl== x.get('direct'))]
                print "list without removed:"
                print imagelist
                writelog("POST / Request to delete file")
                return render.multiimage(imagelist, "Yay, image deleted!")
            else:
                return render.multiimage(imagelist, "Image not found (already deleted?).")
            

        


if __name__ == "__main__":
    app.run()

application = app.wsgifunc()
